-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 27-10-2021 a las 22:32:02
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tsgsdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

DROP TABLE IF EXISTS `compra`;
CREATE TABLE IF NOT EXISTS `compra` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `IdUsuario` int(30) NOT NULL,
  `Fecha` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compravideojuegos`
--

DROP TABLE IF EXISTS `compravideojuegos`;
CREATE TABLE IF NOT EXISTS `compravideojuegos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdCompra` int(11) NOT NULL,
  `IdVideojuego` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desarrollador`
--

DROP TABLE IF EXISTS `desarrollador`;
CREATE TABLE IF NOT EXISTS `desarrollador` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `desarrollador`
--

INSERT INTO `desarrollador` (`Id`, `Nombre`) VALUES
(1, 'Konami');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editor`
--

DROP TABLE IF EXISTS `editor`;
CREATE TABLE IF NOT EXISTS `editor` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `editor`
--

INSERT INTO `editor` (`Id`, `Nombre`) VALUES
(1, 'Konami');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

DROP TABLE IF EXISTS `genero`;
CREATE TABLE IF NOT EXISTS `genero` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`Id`, `Nombre`) VALUES
(1, 'Cartas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Usuario` varchar(30) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Clave` varchar(32) NOT NULL,
  `Email` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Id`, `Usuario`, `Nombre`, `Clave`, `Email`) VALUES
(1, 'Josema23', 'Jose Maria', '81dc9bdb52d04dc20036dbd8313ed055', 'josepe@gmail.com'),
(2, 'Coscu', 'Martin Perez Disalvo', '319cd07d3694e45bc1cd7d13c91a9365', 'coscu@gmail.com'),
(3, 'ElMati', 'Mateo Fernandez', 'a05f63b1a607dcc7a2c30f1285c7865c', 'matife@yahoo.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videojuego`
--

DROP TABLE IF EXISTS `videojuego`;
CREATE TABLE IF NOT EXISTS `videojuego` (
  `Id` int(10) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) NOT NULL,
  `IdDesarrollador` int(10) NOT NULL,
  `IdGenero` int(10) NOT NULL,
  `IdEditor` int(11) NOT NULL,
  `Anio` int(30) NOT NULL,
  `metacritic` decimal(10,2) NOT NULL,
  `Caratula` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `videojuego`
--

INSERT INTO `videojuego` (`Id`, `Nombre`, `IdDesarrollador`, `IdGenero`, `IdEditor`, `Anio`, `metacritic`, `Caratula`) VALUES
(1, 'Duel Links', 1, 1, 1, 2017, '8.10', 'Yu-Gi-Oh!_Duel_Links.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
